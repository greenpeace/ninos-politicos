# How to build he project:

This project uses [Gulp](http://gulpjs.com/) to generate the css/javascript.

To generate the code in the terminal run the following commands:

* `cd path/to/your/downloaded-site`
* `gulp`

## How to install Gulp

* [Install Gulp](http://gulpjs.com/)

You'll need to install gulp's dependencies in the site's folder by running the commands:

* `sudo npm install --save-dev gulp-minify-css`
* `sudo npm install --save-dev gulp-uglify`
* `sudo npm install --save-dev gulp-rename`
* `sudo npm install --save-dev gulp-concat`
