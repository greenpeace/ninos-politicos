<?php

/**
 * API endpoint to sign a petition in Engaging Networks
 *
 */

// --------- Default config file
require_once('config.php');

/* Post request to Engaging Networks */
require_once('signup.php');

function SafeInput ( $msg, $limit ) {
    if ( $msg == "") {
        return $msg;
    }
    $a = filter_var( $msg, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW );
    $b = addslashes ( $a );
    $c = substr($b, 0, $limit );
    return $c;
}

function getPostData() {
	
	$post = array();
	
	$post['last_source_string'] = isset( $_POST['last_source_string'] ) ? SafeInput( $_POST['last_source_string'], 40): '';
	
	$post['first_name'] = isset( $_POST['first_name'] ) ? SafeInput( $_POST['first_name'], 30): '';
	$post['first_surname'] = isset( $_POST['first_surname'] ) ? SafeInput( $_POST['first_surname'], 30): '';
	// $post['second_surname'] = isset( $_POST['second_surname'] ) ? SafeInput( $_POST['second_surname'], 30): '';
	// $post['id_number'] = isset( $_POST['id_number'] ) ? SafeInput( $_POST['id_number'], 10): '';
	$post['email'] = isset( $_POST['email'] ) ? addslashes( filter_var( $_POST['email'], FILTER_SANITIZE_EMAIL ) ) : '';
	// $post['phone_number'] = isset( $_POST['phone_number'] ) ? SafeInput( $_POST['phone_number'], 9) : '';
	// $post['postcode'] = isset( $_POST["postcode"] ) ? SafeInput( $_POST["postcode"], 5) : "";
	$post['privacy'] = isset( $_POST['privacy'] ) ? SafeInput( $_POST['privacy'], 8) : '';

	return $post;
}

function errorsInForm( $postData ) {
    
    $errors = array();

    // first_name
    if ( !$postData['first_name'] ) {
        array_push( $errors, 'first_name');
    }   

    // first_surname
    if ( !$postData['first_surname'] ) {
        array_push( $errors, "first_surname");
    } 
    
    // email
    if ( ( $postData['email']!="" and !filter_var( $postData['email'], FILTER_VALIDATE_EMAIL) ) or ( $postData['email']=="" )  ) {
        array_push( $errors, 'email');
    }
	
    // id_number
    // if ( preg_match( "/^[A-z]?\d{7,8}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]$/" , $postData['id_number'], $matches) == "0"  ) {
    //    array_push( $errors, 'id_number');
    // }

    // phone_number
    //if ( preg_match( "/^[6789]\d{7}\d$/" , $postData['phone_number'], $matches) == "0" and $postData['phone_number'] != "" ) {
    //    array_push( $errors, "phone_number");
    //}
	
    return $errors;
    
}

// --------- PROCESS ---------

// Input
$postData = getPostData();
$errorsInForm = errorsInForm( $postData );

if ( count( $errorsInForm ) == 0 ) {
	
	$data = array(
	'last_source_string' => $postData['last_source_string'],
	'first_name' => $postData['first_name'],
	'first_surname' => $postData['first_surname'],
	// 'second_surname' => $postData['second_surname'],
	// 'id_number' => $postData['id_number'],
	'email' => $postData['email'],
	// 'phone_number' => $postData['phone_number'],
	// 'privacy' => $postData['privacy']
	);
	
	signupEngagingNetworks( $data );
	
	
}

// --------- Output Json
$response = array();
$response['error_count'] = count( $errorsInForm ) ;
$response['errors'] = $errorsInForm;
$response['post'] = $postData;
$response = json_encode( $response );
header('Content-Type: application/json');
// header("Access-Control-Allow-Origin: *" );
echo ( $response );


?>