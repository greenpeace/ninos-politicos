/* ---------- Unsuported SVG ---------- */
if ( Modernizr.svg == false) {
        $(".greenlogo").prop("src", "/assets/ico/greenpeace.png");
}
/* ---------- /Unsuported SVG ---------- */

/* ---------- Cookie alert ---------- */
$(".close").on("click", function() {
    setTheCookie("CookiesAccepted");
    typeof ga == "function" && ga('send', 'event', 'Cookies', 'Esconder', 'Mensaje' );
    typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', 'Cookies', 'Esconder' , 'Mensaje' ]);
});
if ( readCookie("CookiesAccepted") ) {
    $("#cookieMsg").hide();
}
/* ---------- /Cookie alert ---------- */

/* ---------- Signups, tweets and likes counters ---------- */
(function(){
	var thisPageUrl = $("link[rel=canonical]").prop("href");
	if ( $("#facebookShares").length > 0) {
		insertFacebookShares( thisPageUrl, "#facebookShares");
	}
	if ( $("#twitterShares").length > 0) {
		insertTwitterShares( thisPageUrl, "#twitterShares");
	}
})();
/* ---------- /Signups, tweets and likes counters ---------- */

/* ---------- Log form errors in Analytics  ---------- */
$("#mainform").on("submit", function() {
    if ( $("#mainform").valid() == false ) {
        logErrorInAnalytics( campaign_name );
    } 
});
/* ---------- /Log form errors in Analytics  ---------- */

/* ---------- Privacy policy link and tab ---------- */

$('#privacy_policy_text').hide();

$("#privacy_policy_link").on("click", function(event) {
        event.preventDefault();
        $("#privacy_policy_text").toggle();
        typeof ga == "function" && ga('send', 'event', campaign_name, 'PrivacyPolicy', 'Toggle' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'PrivacyPolicy' , 'Toggle' ]);
});

$("#hide_privacy_policy").on("click", function() {
        $('#privacy_policy_text').hide();
        typeof ga == "function" && ga('send', 'event', campaign_name, 'PrivacyPolicy', 'Hide' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'PrivacyPolicy' , 'Hide' ]);
});

$("#privacy_policy_link_footer").on("click", function(){
    typeof ga == "function" && ga('send', 'event', campaign_name, 'PrivacyPolicy', 'ClickFooter' );
    typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'PrivacyPolicy' , 'ClickFooter' ]);
});

/* ---------- /Privacy policy link and tab ---------- */

/* ---------- Get Forms Values ---------- */

function GetFormsValues () {
    var x = {};

	x['formulario'] = $("#formulario").val() ? $("#formulario").val() : "";

    /* ---- PETITION ---- */
    x['last_source_string'] = $("#last_source_string").val() ? $("#last_source_string").val() : "";
    x['first_name'] = $("#first_name").val() ? $("#first_name").val() : "";
    x['first_surname'] = $("#first_surname").val() ?  $("#first_surname").val() : ""; 
    x['email'] = $("#email").val() ? $("#email").val() : "";    
    x['firmo'] = $("#firmo:checked").val() ? $("#firmo:checked").val() : "";
    x['privacy'] = $("#privacy:checked").val() ? $("#privacy:checked").val() : "";
    
    return x
}

/* ---------- /Get Forms Values ---------- */

/* ---------- Form validation ---------- */

window.atempts = 0;

validator = $("#mainform").validate({
	
	submitHandler: function(form) {
	    
	    atempts = atempts + 1;
	    
	    if ( atempts == 1 ) {
			
			$("body").css("cursor", "progress");
			$("button[type=submit]").css("cursor", "progress");
			
			var formValues = GetFormsValues();
			
			jQuery.ajax({
				
				"url" : "api/",
				"type" : "POST",
				"data" : {
					"last_source_string": formValues["last_source_string"],
					"first_name": formValues["first_name"],
					"first_surname": formValues["first_surname"],
					"email": formValues["email"],
					"privacy": formValues["privacy"]
				}
			
			}).done( function(data) {
				
				/* This code executes if the Ajax call works well */
				typeof ga == "function" && ga('send', 'event', campaign_name, 'Firma', 'OK' );
				typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'Firma', 'OK']);
				
			}).always( function() {
				
				$("#petition_form").hide();
				$("#thankyou").show();
				$("body").css("cursor", "default");
				$("button[type=submit]").css("cursor", "default");
				
			}).error( function() {
				
				typeof ga == "function" && ga('send', 'event', campaign_name, 'Firma', 'Error' );
				typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'Firma', 'Error']);
				
			});
			
		}
	},
    
    rules: {
		first_name: {
			es_first_name: true
		},
		first_surname: {
		  es_first_surname: true  
		},
		id_number: {
			es_nifcifnie: true
		},
		phone_number: {
			es_phone: true
		},
		postcode: {
			cpost: true
		}
    },

    messages: {
		nombre_y_apellido: {
			required: "El nombre y apellido por favor"
		},
		provincia: {
			required: "La província por favor"
		},
        first_name: {
            required: "Tu nombre por favor",
            es_first_name: "Tu nombre por favor"
        },  
        first_surname: {
            required: "Tu primero apellido por favor",
            es_first_surname: "Tu primero apellido por favor"
        }, 
        second_surname: {
            required: "Tu segundo apellido por favor"
        },
        email: {
            required: "Tu email por favor",
            email: "Un email válido por favor, ej. nombre@dominio.tld"
        },
        id_number: {
            required: "Un DNI o NIE españoles (sin guiones y sin espacios)",
            es_nifcifnie:"Un DNI o NIE españoles. Por ejemplo 82451384H o X6909535J (sin guiones y sin espacios)"
        },
        phone_number: {
            required: "Tu número de teléfono por favor",
            es_phone: "Número de un fijo o móvil español valido. Ej. 622345679 (sin guiónes y sin espacios)"
            
        },
		
		postcode: {
			cpost: "Un número de código postal español con 5 dígitos"
		},
        
        privacy: {
            required: "Falta aceptar la política de privacidad"
        }
    },       
    
    
});

/* ---------- /Form validation  ---------- */

/* ---------- Last source form field ---------- */

(function() {
        
    if ( location.hash.search("notrack") == -1 ) {
        
        if ( window.location.search !="") {
            $("input[name='last_source_string']").val( window.location.search );
            if ( Modernizr.sessionstorage == true) {
                sessionStorage.setItem("last_source_string", window.location.search );
            }
        } else {
            if ( Modernizr.sessionstorage == true && sessionStorage.getItem("last_source_string") != null ) {
                var last_source_from_ss = sessionStorage.getItem("last_source_string");
                $("input[name='last_source_string']").val( last_source_from_ss );
            }                
        }
    }    
})();

/* ---------- /Last source form field ---------- */

/* ----------  Disable mandatory phone number for special purposes  ----------*/

function disable_phone_number() {
    if ( location.hash.search("notel") >= 0) {
        $("#phone_number").removeProp("required");
        $("#phone_number").rules("remove", "required");
        $("label[for='phone_number']").text("Teléfono");
    }
}

window.onhashchange = function() {
    disable_phone_number();
	showPhotoModal();
};

disable_phone_number();

/* ----------  /Disable mandatory phone number for special purposes  ----------*/

/* ---------- Tracking clicks in the donate button and social sharing buttons in Google Analytics   ---------- */

$("body").on("click", "#GoToDonationForm", function(){
        typeof ga == "function" && ga('send', 'event', campaign_name, 'Click', 'DonateButton' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'Click', 'DonateButton' ]);
        
        window.location.href = $(this).data("click");
});

$("body").on("click", "#facebook", function() {
        typeof ga == "function" && ga('send', 'event', campaign_name, 'SocialShare', 'Facebook' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'SocialShare', 'Facebook' ]);
});

$("body").on("click", "#twitter", function() {
        typeof ga == "function" && ga('send', 'event', campaign_name, 'SocialShare', 'Twitter' );    
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent',  campaign_name, 'SocialShare', 'Twitter' ]);
});

$("body").on("click", "#googleplus", function() {
        typeof ga == "function" && ga('send', 'event', campaign_name, 'SocialShare', 'GooglePlus' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'SocialShare', 'GooglePlus' ]);				
});

$("body").on("click", "#whatsapp", function() {
        typeof ga == "function" && ga('send', 'event', campaign_name, 'SocialShare', 'GooglePlus' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'SocialShare', 'Whatsapp' ]);				
});

/* ---------- /Tracking clicks in the donate button and social sharing buttons in Google Analytics   ---------- */

/* ---------- Other Analytics ----------  */
$(".dropdown-toggle").on("click", function(){
		typeof ga == "function" && ga('send', 'event', campaign_name, 'Dropdown', 'Mas_informacion' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'Dropdown', 'Mas_informacion' ]);
});
$(".navbar-brand, #link_home").on("click", function(){
		typeof ga == "function" && ga('send', 'event', campaign_name, 'Home', 'Click' );
        typeof (_gaq ) == "object" && _gaq.push(['_trackEvent', campaign_name, 'Home', 'Click' ]);	
});
/* ---------- /Other Analytics ----------  */

/* ---------- Photo widget ----------  */

$(".photo-widget a").on("click", function(e) {
	e.preventDefault();
	var element = $(this).data("hash");
	window.location.hash = element;
});

function showPhotoModal () {
	var fileName;
	var element = window.location.hash;
	
	// From the hash get the html file name
	switch ( element ) {
		case "#Mariano":
			fileName = "Mariano.html";
		break;
		case "#Pedro":
			fileName = "Pedro.html";
		break;
		case "#Pablo":
			fileName = "Pablo.html";
		break;
		case "#Albert":
			fileName = "Albert.html";
		break;
		case "#Alberto":
			fileName = "Alberto.html";
		break;
		default:
			fileName = "";
		break;
	}
	
	if (fileName != "") {
		// Open a modal box with a closing button (check Bootstap)
		$('#photoModal').modal( {
			keyboard: true
		});
	}

	
	// Load from .html file:
	//		the social media buttons #socialBox
	$( "#socialBoxContainer" ).load( fileName + " #socialBox" );
	//		Theimage #socialBox

	$("#politicianImageContainer").load( fileName + " #largePortrait picture", function() {
		picturefill();
	} );
	
}

showPhotoModal ();

$('#photoModal').on('hidden.bs.modal', function () {
	window.location.hash = "Candidatos";
	$("#politicianImageContainer").empty();
	$( "#socialBoxContainer" ).empty();
})

/* ---------- Fix buttons ----------  */

$("#socialBoxContainer").on("click", ".socialsharebutton, .homebutton", function(){
	
	var jumpTo = $(this).find("a").prop("href");
	window.location.href = jumpTo;
	
});

$(".profile, .petition").on("click", ".socialsharebutton, .homebutton", function(){
	
	var jumpTo = $(this).find("a").prop("href");
	window.location.href = jumpTo;
	
});


$("#socialBoxContainer").on("click", "a", function(event){
	event.preventDefault();
});

$(".profile, .petition").on("click", ".socialsharebutton a, .homebutton a", function(event){
	event.preventDefault();
});